<?php

require __DIR__ . '/../vendor/autoload.php';

$client = \Majak\LightBeam\DefaultLightBeamClient::factory();

$amount = 100000;
$entries = [];

$start = microtime(true);
for ($i = 1; $i < $amount; $i++) {
    $identifier = $client->nextId();
    if (isset($entries[$identifier])) {
        $lapsed = microtime(true) - $start;
        echo 'Collision as attempt #' . $i . "\n";
        echo 'Time passed ' . $lapsed;
        exit(1);
    }

    $entries[$identifier] = true;
//    usleep(500);

    echo  $identifier . "\n";
}
$lapsed = microtime(true) - $start;

echo 'Sequences ' . $i . "\n";
echo 'Time passed ' . $lapsed;
