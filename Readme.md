# Light Beam

A distributed unique ID generator inspired by 
[Snowflake](https://github.com/twitter-archive/snowflake/blob/b3f6a3c6ca8e1b6847baa6ff42bf72201e2c2231/src/main/scala/com/twitter/service/snowflake/IdWorker.scala), 
[Sonyflake](https://github.com/sony/sonyflake/blob/master/sonyflake.go), 
[Pinterest](https://medium.com/@Pinterest_Engineering/sharding-pinterest-how-we-scaled-our-mysql-fleet-3f341e96ca6f), 
[Instagram](https://instagram-engineering.com/sharding-ids-at-instagram-1cf5a71e5a5c), 
[Baidu](https://github.com/baidu/uid-generator) and 
[ChronoId](https://github.com/middag/chrono-id).


`DefaultLightBeamClient` generates 64 bit integer.
