<?php

declare(strict_types=1);


namespace Majak\LightBeam;


interface SequenceProvider
{
    /**
     * @param int $size
     *
     * @return int
     */
    public function getNumber(int $size): int;
}
