<?php

declare(strict_types=1);

namespace Majak\LightBeam;


interface LightBeamClient
{
    /**
     * @return int
     */
    public function nextId(): int;

    /**
     * @param int $lightBeam
     *
     * @return int[]
     */
    public function decode(int $lightBeam): array;
}
