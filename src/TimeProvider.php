<?php

declare(strict_types=1);


namespace Majak\LightBeam;


interface TimeProvider
{
    /**
     * @return int
     */
    public function getTime(): int;
}