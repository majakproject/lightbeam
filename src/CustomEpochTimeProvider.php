<?php

declare(strict_types=1);


namespace Majak\LightBeam;


class CustomEpochTimeProvider implements TimeProvider
{
    private const PRECISION = 1000;

    /**
     * @var int
     */
    private $epoch = 1299020552 * self::PRECISION;

    /**
     * @inheritdoc
     */
    public function getTime(): int
    {
        return (int)round(microtime(true) * self::PRECISION) - $this->epoch;
    }
}
