<?php

declare(strict_types=1);


namespace Majak\LightBeam;


class StaticShardProvider implements ShardProvider
{
    /**
     * @var int
     */
    private $number;

    /**
     * @param int $number
     */
    public function __construct(int $number)
    {
        $this->number = $number;
    }

    /**
     * @inheritdoc
     */
    public function getNumber(): int
    {
        return $this->number;
    }
}
