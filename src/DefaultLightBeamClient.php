<?php

declare(strict_types=1);


namespace Majak\LightBeam;

class DefaultLightBeamClient implements LightBeamClient
{
    private const TOTAL_SIZE = 64;
    private const TIME_BLOCK_SIZE = 42;
    private const SHARD_BLOCK_SIZE = 10;
    private const SEQUENCE_BLOCK_SIZE = 12;

    private const TIME_SHIFT = self::TOTAL_SIZE - self::TIME_BLOCK_SIZE;
    private const SHARD_SHIFT = self::TIME_SHIFT - self::SHARD_BLOCK_SIZE;
    private const SEQUENCE_SHIFT = self::SHARD_SHIFT - self::SEQUENCE_BLOCK_SIZE;

    /**
     * @var TimeProvider
     */
    private $timeProvider;

    /**
     * @var ShardProvider
     */
    private $shardProvider;

    /**
     * @var SequenceProvider
     */
    private $suffixProvider;

    /**
     * @param TimeProvider     $timeProvider
     * @param ShardProvider    $shardProvider
     * @param SequenceProvider $suffixProvider
     */
    public function __construct(TimeProvider $timeProvider, ShardProvider $shardProvider, SequenceProvider $suffixProvider)
    {
        $this->timeProvider = $timeProvider;
        $this->shardProvider = $shardProvider;
        $this->suffixProvider = $suffixProvider;
    }

    /**
     * @return DefaultLightBeamClient
     */
    public static function factory(): DefaultLightBeamClient
    {
        $timeProvider = new CustomEpochTimeProvider();
        $shardProvider = new StaticShardProvider(1);
        $suffixProvider = new RandomSequenceProvider();

        return new self($timeProvider, $shardProvider, $suffixProvider);
    }

    /**
     * @inheritdoc
     */
    public function nextId(): int
    {
        $time = $this->timeProvider->getTime();
        $shard = $this->shardProvider->getNumber();
        $sequence = $this->suffixProvider->getNumber(self::SEQUENCE_BLOCK_SIZE);

        $flake = $this->generate($time, $shard, $sequence, self::TIME_SHIFT, self::SHARD_SHIFT);

        if ($flake < 0) {
            throw new \RuntimeException(sprintf('Too long integer %s. Try to increase size og time block.', $flake));
        }

        return $flake;
    }

    /**
     * @inheritdoc
     */
    public function decode(int $lightBeam): array
    {
        return [
            ($lightBeam >> self::TIME_SHIFT) & bindec(str_repeat('1', self::TIME_BLOCK_SIZE)),
            ($lightBeam >> self::SHARD_SHIFT) & bindec(str_repeat('1', self::SHARD_BLOCK_SIZE)),
            ($lightBeam >> self::SEQUENCE_SHIFT) & bindec(str_repeat('1', self::SEQUENCE_BLOCK_SIZE)),
        ];
    }

    /**
     * @param int $time
     * @param int $shard
     * @param int $sequence
     * @param int $timeShift
     * @param int $shardShift
     *
     * @return int
     */
    protected function generate(int $time, int $shard, int $sequence, int $timeShift, int $shardShift): int
    {
        if (mb_strlen(decbin($time)) > self::TIME_BLOCK_SIZE) {
            throw new \RuntimeException(sprintf('The current time (%d) does not fit in a block that is limited to %d bits', $time, self::TIME_BLOCK_SIZE));
        }

        if (mb_strlen(decbin($shard)) > self::SHARD_BLOCK_SIZE) {
            throw new \RuntimeException(sprintf('The value %d does not fit in a block that is limited to %d (%d bits)', $shard, bindec(str_repeat('1', self::SHARD_BLOCK_SIZE)), self::SHARD_BLOCK_SIZE));
        }

        if (mb_strlen(decbin($sequence)) > self::SEQUENCE_BLOCK_SIZE) {
            throw new \RuntimeException(sprintf('The value %d does not fit in a block that is limited to %d (%d bits)', $shard, bindec(str_repeat('1', self::SEQUENCE_BLOCK_SIZE)), self::SEQUENCE_BLOCK_SIZE));
        }

        return $time << $timeShift
            | $shard << $shardShift
            | $sequence << 0;
    }
}
