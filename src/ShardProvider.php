<?php

declare(strict_types=1);

namespace Majak\LightBeam;


interface ShardProvider
{
    /**
     * @return int
     */
    public function getNumber(): int;
}
