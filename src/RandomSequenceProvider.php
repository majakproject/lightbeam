<?php

declare(strict_types=1);


namespace Majak\LightBeam;


class RandomSequenceProvider implements SequenceProvider
{
    /**
     * @var int
     */
    private static $counter;

    /**
     * @inheritdoc
     *
     * @throws \Exception
     */
    public function getNumber(int $size): int
    {
        $randomSize = (int)floor($size / 2);

        $maximum = bindec(str_repeat('1', $size));

        $randomMaximum = bindec(str_repeat('1', $randomSize));
        $counterMaximum = $maximum - $randomMaximum;

        if (null === self::$counter || self::$counter >= $counterMaximum) {
            self::$counter = -1;
        }

        ++self::$counter;

        $number = random_int(0, $randomMaximum) << ($size - $randomSize);
        $number |= self::$counter;

        return $number;
    }
}
